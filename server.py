import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# process command line arguments
argc = len(sys.argv)
if argc != 2:
    print ("usage: python3 server.py serverPort")
    sys.exit()
port = int(sys.argv[1])

# Bind the socket to the port
server_address = ('',port)
print ('starting up on {} port {}'.format(server_address[0], server_address[1]))

sock.bind(server_address)

# Listen for incoming connections
sock.listen()

while True:
    # Wait for a connection
    print ('waiting for a connection')
    connection, client_address = sock.accept()

    print ('connection from {}'.format(client_address))

    # Receive the data in small chunks and retransmit it.  Continue doing this
    # until the client closes the tcp connection
    while True:
        data = connection.recv(16)
        print ('received "{}"'.format(data.decode()))
        print ('length of data={}'.format(len(data)))

        if len(data) > 0:
            print ('sending data back to the client')
            connection.sendall(data)
        else:
            print ('{} has closed the connection, so we are finished.'.format(client_address))

            break

    # Clean up the connection
    connection.close()
