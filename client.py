import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
# process command line arguments
argc = len(sys.argv)
if argc != 3:
    print ("usage: python3 client.py serverAddress serverPort")
    sys.exit()
serverName = sys.argv[1]
port = int(sys.argv[2])

server_address = (serverName,port)
print ('connecting to {} port {}'.format(server_address[0], server_address[1]) )
sock.connect(server_address)

# Send data
message = 'This is the message.  It will be repeated.'
print ('sending "{}"'.format(message))
sock.sendall(message.encode())

# Look for the response
amount_received = 0
amount_expected = len(message)

while amount_received < amount_expected:
    data = sock.recv(16)
    amount_received += len(data)
    print ('received "{}"'.format(data.decode()))

dummy = input("Press enter to terminate ")
print ('closing socket')
sock.close()
