import socket
import sys
import threading

# define a class to handle one connected client.
class serverThread( threading.Thread ):
    #initialize the instance.  The socket and clent address are passed
    def __init__(self, connection, clientAddress ):
        threading.Thread.__init__(self)
        self.connection = connection
        self.clientAddress = clientAddress

    # this method (which is automatically invoked by start) does all the processing for one client
    def run(self):
        print ('connection from {}'.format(self.clientAddress))
        # Receive the data in small chunks and retransmit it.  Continue doing this
        # until the client closes the tcp connection
        while True:
            data = self.connection.recv(16)
            if len(data) > 0:
                self.connection.sendall(data)
            else:
                print ('{} has closed the connection, so we are finished.'.format(self.clientAddress))
                break

        # Clean up the connection
        self.connection.close()

# the main function
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# process command line arguments
argc = len(sys.argv)
if argc != 2:
    print ("usage: python3 threaded-server.py serverPort")
    sys.exit()
port = int(sys.argv[1])

# Bind the socket to the port
server_address = ('',port)

print ('starting up on {} port {}'.format(server_address[0], server_address[1]))
sock.bind(server_address)

# Listen for incoming connections
sock.listen()

while True:
    # Wait for a connection
    print ('waiting for a connection')
    connection, client_address = sock.accept()

    # create a thread to handle this client
    thread = serverThread( connection, client_address )
    thread.start()
